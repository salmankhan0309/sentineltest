<?php include 'include/header.php' ?>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="my-3">
                    <div class="bg_green p-2 d-inline-block white_text">
                        LATEST NEWS
                    </div>
                    <div class="d-inline-block ml-3">
                        Richmond Christmas Fund host Thank-You Breakfast
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <img src="images/home/hm_slider.jpg" width="100%">
                    </div>
                    <div class="col-md-12">
                        <h4 class="my-2 text-uppercase">Richmond Christmas hosts thank-you breakfast</h4>
                        <p class="mb-4">There was pleanty of celebrating and smiles Wednesday morning when Richmond Cares.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src="images/home/ad_m.jpg" width="100%">
                    </div>
                </div>
                <!--- Top stories Start ---->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-uppercase darkblue_text my-4">top stories</h4>
                    </div>
                </div>
                <div class="row">
                    <!--- Community Start ---->
                    <div class="col-md-4">
                        <!--- Community Head ---->
                        <div class="w-100 d-flex align-items-end border-bottom">
                            <div class="bg_lightblue p-2 d-inline-block white_text text-uppercase text-10">
                                community
                            </div>
                            <div class="d-inline-block ml-auto grey_text font-weight-light text-11">
                                See All
                            </div>
                        </div>
                        <!--- Community Head ---->
                        <!--- Community Stories ---->



                            <div class="w-100 mt-3">
                                <?php for( $i = 0; $i <= 3; $i++){?>
                                <img src="images/home/tbn_top_stories.jpg" width="100%">
                                <div class="text-justify my-3 text-11 height-65">
                                    City wants to hear what community wishes to do with old aquatic center.
                                </div>
                                <?php }?>
                            </div>

                        <div class="w-100 border-top ">
                            <div class="text-justify text-11 my-3">
                                City wants to hear what community wishes to do with old aquatic center.
                            </div>
                        </div>
                        <div class="w-100 border-top border-bottom ">
                            <div class="text-justify text-11 my-3">
                                City wants to hear what community wishes to do with old aquatic center with extra text.
                            </div>
                        </div>
                        <!--- Community Stories ---->
                    </div>
                    <!--- Community End ---->

                    <!--- Canada Start ---->
                    <div class="col-md-4">
                        <!--- Canada Head ---->
                        <div class="w-100 d-flex align-items-end border-bottom">
                            <div class="bg_lightblue p-2 d-inline-block white_text text-uppercase text-10">
                                canada
                            </div>
                            <div class="d-inline-block ml-auto grey_text font-weight-light text-11">
                                See All
                            </div>
                        </div>
                        <!--- Canada Head ---->
                        <!--- Canada Stories ---->
                            <div class="w-100 mt-3">
                                <?php for( $i = 0; $i <= 3; $i++){?>
                                <img src="images/home/tbn_top_stories.jpg" width="100%">
                                <div class="text-justify my-3 text-11 height-65">
                                    City wants to hear what community wishes to do with old aquatic center and some more text with cropped text.
                                </div>
                                <?php } ?>
                            </div>
                        <div class="w-100 border-top">
                            <div class="text-justify my-3 text-11">
                                City wants to hear what community wishes to do with old aquatic center.
                            </div>
                        </div>
                        <div class="w-100 border-top border-bottom">
                            <div class="text-justify my-3 text-11 ">
                                City wants to hear what community wishes to do with old aquatic center and some more text with cropped text.
                            </div>
                        </div>
                        <!--- Canada Stories ---->
                    </div>
                    <!--- Canada End ---->

                    <!--- International Start ---->
                    <div class="col-md-4">
                        <!--- International Head ---->
                        <div class="w-100 d-flex align-items-end border-bottom">
                            <div class="bg_lightblue p-2 d-inline-block white_text text-uppercase text-10">
                                International
                            </div>
                            <div class="d-inline-block ml-auto grey_text font-weight-light text-11">
                                See All
                            </div>
                        </div>
                        <!--- International Head ---->
                        <!--- International Stories ---->
                            <div class="w-100 mt-3">
                                <?php for( $i = 0; $i <= 3; $i++){?>

                                <img src="images/home/tbn_top_stories.jpg" width="100%">
                                <div class="text-justify my-3 text-11 height-65">
                                    City wants to hear what community wishes to do with old aquatic center and some more text.
                                </div>
                                <?php } ?>
                            </div>
                        <div class="w-100 border-top">
                            <div class="text-justify my-3 text-11 ">
                                City wants to hear what community wishes to do with old aquatic center.
                            </div>
                        </div><div class="w-100 border-top border-bottom">
                            <div class="text-justify my-3 text-11">
                                City wants to hear what community wishes to do with old aquatic center and some more text.
                            </div>
                        </div>

                        <!--- International Stories ---->
                    </div>
                    <!--- International End ---->
                </div>
                <!--- Top stories End ---->

            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <img src="images/home/ad_box.jpg" width="100%">
                    </div>
                </div>
                <div class="w-100 border-bottom mt-4">
                    <div class="bg_green p-2 d-inline-block white_text text-10">
                        POPULAR
                    </div>
                </div>
                <?php for( $i = 0; $i <= 26; $i++){?>
                    <div class="w-100 mt-3">
                        <div class="d-inline-block tbn-popular">
                            <img src="images/home/tbn_popular.jpg" width="100%">
                        </div>
                        <div class="d-inline-block heading-popular">
                            <p class="text-justify text-10 mb-0">Former Liberal justice member walks to Parliament Hill.</p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>

<div class="container-fluid my-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="images/home/ad_l.jpg" width="100%">
            </div>
        </div>
        <!--- Videos you should watch ------>
        <div class="row">
            <div class="col-md-12">
                <div class="border-bottom my-4">
                    <h4 class="text-uppercase darkblue_text my-0 d-inline-block">VIDEOS YOU SHOULD SEE</h4>
                    <div class="d-inline-block ml-5 grey_text font-weight-light text-11">
                        See All Videos
                        <i class="fab fa-youtube"></i>
                    </div>
                </div>
            </div>
        </div>
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-98 mx-auto" role="listbox">
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x active">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img1" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img2" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img3" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img4" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img5" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img6" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img7" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img8" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img9" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-5x">
                    <img src="images/home/tbn_video.jpg" class="img-fluid mx-auto d-block" alt="img10" width="100%">
                    <div class="height-95 text-justify">
                        Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                <i class="fas fa-caret-left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                <i class="fas fa-caret-right"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!--- Videos you should watch ------>
        <!--- Snapshot ------>
        <div class="row">
            <div class="col-md-12">
                <div class="border-bottom my-4">
                    <h4 class="text-uppercase darkblue_text my-0 d-inline-block">SNAPSHOT FROM YOUR COMMUNITY</h4>
                    <div class="d-inline-block ml-5 grey_text font-weight-light text-11">
                        See All Videos
                        <i class="fas fa-camera"></i>                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x active">
                <img src="images/home/tbn_photo.jpg" class="img-fluid mx-auto d-block" alt="img1" width="100%">
                <p class="text-justify">Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa</p>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_photo.jpg" class="img-fluid mx-auto d-block" alt="img2" width="100%">
                <p>Former Liberal justice member Parliament Hill in Ottawa</p>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_photo.jpg" class="img-fluid mx-auto d-block" alt="img3" width="100%">
                <p>Former Liberal justice member Jody Wilson-Raybould walks to Parliament Hill in Ottawa</p>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_photo.jpg" class="img-fluid mx-auto d-block" alt="img4" width="100%">
                <p>Former Liberal walks to Parliament Hill in Ottawa</p>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_photo.jpg" class="img-fluid mx-auto d-block" alt="img5" width="100%">
                <p>Former Liberal justice Hill in Ottawa</p>
            </div>
        </div>
        <!---- snapshot ends --->
        <!---- Couponzone starts --->

        <div class="row">
            <div class="col-md-12">
                <div class=" my-4">
                    <img class="my-0 d-inline-block" src="images/home/logo_couponzone_hm.png" />
                    <img class="my-0 d-inline-block ml-5" src="images/home/logo_couponzone_shownsafe_hm.png" />

                    <div class="d-inline-block ml-5 grey_text font-weight-light text-11">
                        See All Coupons
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x active">
                <img src="images/home/tbn_coupon.jpg" class="img-fluid mx-auto d-block" alt="img1" width="100%">
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_coupon.jpg" class="img-fluid mx-auto d-block" alt="img2" width="100%">
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_coupon.jpg" class="img-fluid mx-auto d-block" alt="img3" width="100%">
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_coupon.jpg" class="img-fluid mx-auto d-block" alt="img4" width="100%">
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5x">
                <img src="images/home/tbn_coupon.jpg" class="img-fluid mx-auto d-block" alt="img5" width="100%">
            </div>
        </div>
        <!---- Couponzone ends --->

    </div>
</div>

<?php include 'include/footer.php'?>
