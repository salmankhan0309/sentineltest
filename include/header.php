<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
    <!---style sheet-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


</head>
<body>
<nav class="navbar navbar-expand-xl navbar-light bg_darkblue nav-position">
    <div class="container">
    <a class="navbar-brand pl-0" href="#"><img src="images/home/logo_richmondsentinel.png" width="100%"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse w-100 flex-md-column" id="navbarCollapse">
        <ul class="navbar-nav w-100 nav-line ">
            <li class="nav-item active">
                <a class="nav-link py-1 white_text" href="#">home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">community</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">canada</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">international</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">videos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">couponzone</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">edition</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 white_text" href="#">place an ad</a>
            </li>
        </ul>
    </div>
    </div>
</nav>
<div class="w-100 bg_black">
    <div class="container">
        <form class="form-inline ">
            <span class="nav-subtitle lightblue_text text-11">GET ALL YOUR NEWS UPDATES IN ONE PLACE!</span>
            <ul class="socialmedia-list">
                <li class="socialmedia-icons">
                    <img src="images/home/icon_fb.png" />
                </li>
                <li class="socialmedia-icons">
                    <img src="images/home/icon_twitter.png" />
                </li>
                <li class="socialmedia-icons">
                    <img src="images/home/icon_instagram.png" />
                </li>
                <li class="socialmedia-icons">
                    <img src="images/home/icon_youtube.png" />
                </li>
            </ul>
            <div class="input-group ml-auto">
                <input type="text" class="form-control border-dark" placeholder="Enter Keywords">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>

    </div>
</div>
