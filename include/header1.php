<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
    <!---style sheet-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!----scripts--->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-xl navbar-light bg-light">
    <a class="navbar-brand" href="#"><img src="images/logo_sentinel.png" width="200"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse w-100 flex-md-column" id="navbarCollapse">
        <form class="form-inline w-100">
            <span class="nav-subtitle">GET ALL YOUR NEWS UPDATES IN ONE PLACE!</span>
            <div class="input-group ml-auto">
                <input type="text" class="form-control border-dark" placeholder="Search">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
        <ul class="navbar-nav w-100 nav-line">
            <li class="nav-item active">
                <a class="nav-link py-1" href="#">home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">community</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">canada</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">videos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">couponzone</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">edition</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1" href="#">book an ad</a>
            </li>
            <ul class="navbar-nav nav-follow">
                <li class="nav-item">
                    <i class="fab fa-facebook-square"></i>
                </li>
                <li class="nav-item">
                    <i class="fab fa-twitter-square"></i>
                </li>
                <li class="nav-item">
                    <i class="fab fa-instagram"></i>
                </li>
                <li class="nav-item">
                    <i class="fab fa-youtube-square"></i>
                </li>
            </ul>
        </ul>
    </div>
</nav>


