<footer class="page-footer bg_darkblue">
    <div class="container">
        <div class="row">
            <div class="col-md-7 mt-3 text-11 text-white">
                    © 2017 The Richmond Sentinel. All rights reserved. Designed by Intelli Management Group.
            </div>
            <div class="col-md-5">
                        <ul class="list-inline my-3 float-right">
                            <li class="list-inline-item">
                                <a class="" href="#">About Us</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="#">Contact Us</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="#">Terms of Use</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="#">Privacy Policy</a>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>

</footer>
<!----scripts--->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="assets/js/script.js"></script>
</body>
</html>